<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit8dbf8c16bb0b2285e1b312565e950b94
{
    public static $prefixLengthsPsr4 = array (
        'c' => 
        array (
            'controller\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'controller\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Controllers',
        ),
    );

    public static $classMap = array (
        'controller\\Base' => __DIR__ . '/../..' . '/Controllers/Base.php',
        'controller\\Person' => __DIR__ . '/../..' . '/Controllers/Person.php',
        'controller\\User' => __DIR__ . '/../..' . '/Controllers/User.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit8dbf8c16bb0b2285e1b312565e950b94::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit8dbf8c16bb0b2285e1b312565e950b94::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit8dbf8c16bb0b2285e1b312565e950b94::$classMap;

        }, null, ClassLoader::class);
    }
}
