<?php

    namespace controller;

use mysqli;

    class Person extends Base {

        public function index() {
            echo 'This is index Method of Person Class';
        }

        public function add ($args) {
            mysqli_query($this->connection, "INSERT INTO person (nickName, position) VALUES ('$args[0]','$args[1]')");
            echo $args[0] . ' ' . $args[1] . ' '  . 'added to database';
        }

        public function get () {
            $res = mysqli_query($this->connection, "SELECT * FROM person");
            $this->render($res);
        }
    }

?>