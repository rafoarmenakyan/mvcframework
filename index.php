<?php

use controller\Person;
use controller\User;

require_once realpath("vendor/autoload.php");


if (isset($_GET['url'])) {

        $url = $_GET['url'];
        $url = trim($url);
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $url = explode("/", $url);

        $controllerName = $url[0];
        $methodName = $url[1];

        $params = [];
        for ($i = 2; $i < count($url); $i++) {
           if ($url[$i]) {
               $params[] = $url[$i];
           }
        }

        $fileName = 'controller\\' . $controllerName;
        
        if (class_exists($fileName)) {
           $class = new $fileName;
           if (method_exists($fileName,$methodName)) {
              $class -> $methodName($params);
           } else if($methodName == '') {
               $class -> index();
           } else {
            echo "The Class" . " " . $controllerName . " " . "does not have method of" . " " . $methodName . " " . "name";
           }
        } else {
            echo "Class" . " " . $controllerName . " " . "does not exist";
        }
       
    }

    
?>